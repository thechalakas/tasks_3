﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Tasks_3
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            //here I am going to try wait all 

            //creating a task array with 5 tasks
            Task[] set_of_tasks = new Task[5];

            set_of_tasks[0] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks[1] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks[2] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks[3] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks[4] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });


            //so by now, all 5 tasks are alive

            //lets wait for them

            Task.WaitAll(set_of_tasks);

            Console.WriteLine(" --------------------------------------------thread id is {0}", Thread.CurrentThread.ManagedThreadId);

            //now lets try the wait any task feature to see when a task has done completion

            //an array of tasks again
            Task[] set_of_tasks_2 = new Task[5];

            //lets create some new tasks and assign them to the task array
            set_of_tasks_2[0] = Task.Run(() =>
            {
                Thread.Sleep(5000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks_2[1] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks_2[2] = Task.Run(() =>
            {
                Thread.Sleep(2000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks_2[3] = Task.Run(() =>
            {
                Thread.Sleep(4000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            set_of_tasks_2[4] = Task.Run(() =>
            {
                Thread.Sleep(3000);
                Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            });

            //now I am going to use a while loop to see which task finished first
            while(set_of_tasks_2.Length >0)
            {
                //grab the index of the task that completed from the entire array
                int task_that_completed = Task.WaitAny(set_of_tasks_2);

                Console.WriteLine("Complted task is {0} ", task_that_completed);
                Console.WriteLine(" array task thread id is {0}", Thread.CurrentThread.ManagedThreadId);

                //let me remove the completed task from the task array
                //otherwise, we will never get out of this while loop
                var temporary_list = set_of_tasks_2.ToList();
                temporary_list.RemoveAt(task_that_completed);
                set_of_tasks_2 = temporary_list.ToArray();
            }


            Console.WriteLine(" thread id is {0}", Thread.CurrentThread.ManagedThreadId);
            Console.ReadLine();
        }
    }
}
